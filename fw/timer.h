#ifndef _TIMER_H_
#define _TIMER_H_

#include <stdint.h>

//! Timestamp type
typedef uint64_t Timestamp;

//! Initialize time module
void timer_init();

/*! Get current timestamp
 *  \return Current timestamp
 */
Timestamp timer_timestamp();

/*! Check whether timestamp has passed
 *  \param timestamp Timestamp to be compared with system time
 *  \return 1 when passed
 */
uint8_t timer_hasPassed(Timestamp timestamp);

/*! Get future timestamp milliseconds from now
 *  \param milli Milliseconds from now
 *  \return Future timestamp
 */
Timestamp timer_getFutureMilli(uint32_t milli);

/*! Get interval between two timestamps
 *  \param ts1 First timestamp
 *  \param ts2 Second timestamp (ts2 >= ts1)
 *  \return Interval in milliseconds
 */
uint32_t timer_intervalMilli(Timestamp ts1, Timestamp ts2);


#endif // _TIMER_H_
