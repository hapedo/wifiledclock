#include "settings.h"
#include <string.h>
#include <avr/eeprom.h>
#include "utils.h"

int settings_read(Settings *settings)
{
    if (settings == 0)
        return -1;
    eeprom_read_block(settings, 0, sizeof(Settings));
    uint16_t checksum = settings->checksum;
    settings->checksum = 0;
    if (calculateChecksum((uint8_t*)settings, sizeof(Settings)) == checksum)
        return 0;
    else
        return -1;
}

void settings_writeDefaults(Settings *settings)
{
    Settings defaults;
//    memset(defautls.wifiPass, 0, SETTINGS_WIFIPASS_LENGTH + 1);
    strcpy(defaults.wifiSsid, "HapNet");
    strcpy(defaults.wifiPass, "todd5lon");
    strcpy(defaults.ntpServer, "0.pool.ntp.org");
    defaults.timeZone = 1;
    defaults.daylightSavingTime = 1;
    defaults.checksum = 0;
    defaults.checksum = calculateChecksum((uint8_t*)&defaults, sizeof(Settings));
    eeprom_write_block((uint8_t*)&defaults, 0, sizeof(Settings));
    if (settings)
        *settings = defaults;
}

void settings_write(Settings *settings)
{
    if (settings == 0)
        return;
    settings->checksum = 0;
    settings->checksum = calculateChecksum((uint8_t*)settings, sizeof(Settings));
    eeprom_write_block((uint8_t*)settings, 0, sizeof(Settings));
}
