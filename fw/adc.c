#include "adc.h"
#include "platform.h"
#include "timer.h"
#include "display.h"

#define MEAS_PERIOD			100

#define R_DARK				20
#define R_LIGHT				7
#define R5					100
#define VMAX				1023

static void adc_startMeasure(uint8_t channel);
static uint16_t adc_getResult();
static Timestamp timer;

void adc_init()
{
	timer = 0;
}

void adc_process()
{
	if (timer_hasPassed(timer))
	{
		timer = timer_getFutureMilli(MEAS_PERIOD);
		adc_startMeasure(7);
		uint16_t voltage = adc_getResult();
		float r = R5 * (VMAX - (float)voltage) / (float)voltage;
		if (r >= R_LIGHT)
			r-= R_LIGHT;
		else
			r = 0;
		r /= (R_DARK - R_LIGHT);
		if (r >= 1)
			r = 1;
		r = 1 - r;
		r *= DISPLAY_BRIGHTNESS_MAX;
		if (r == 0)
			r = 1;
		display_setBrightness((uint8_t)r);
	}
}

static void adc_startMeasure(uint8_t channel)
{
    ADMUX = 0x40 | (channel & 0x07);
	ADCSRA = 0x96;
    ADCSRA |= 0x40;
}

static uint16_t adc_getResult()
{
	while((ADCSRA & 0x40))
	{
	}
	uint16_t result = ADCL;
	result |= ADCH << 8;
	ADCSRA = 0x00;
    return result;
}


