#include "uart_usb.h"
#include "config.h"
#include <avr/io.h>
#include <avr/interrupt.h>
#include <string.h>
#include <stdint.h>
#include "settings.h"
#include "utils.h"

#if (USE_USB == 1)

#define RX_BUFFER_SIZE          32
#define TX_BUFFER_SIZE          128
#define RX_MAX_STR_SIZE			32

typedef enum
{
    RXS_WAIT_CMD,
    RXS_SSID,
	RXS_PASS,
	RXS_ZONE,
	RXS_NTP,
	RXS_SHOW_ALL,
	RXS_GET_STR
} RxState;

static char rxBuffer[RX_BUFFER_SIZE];
static char txBuffer[TX_BUFFER_SIZE];
static char rxString[RX_MAX_STR_SIZE];
static uint16_t rxStrSize;
static uint16_t rxStart;
static uint16_t rxEnd;
static uint16_t txStart;
static uint16_t txEnd;
static RxState rxState;
static RxState rxPrevState;


static uint16_t rxAvailable()
{
    UCSR1B &= ~(1 << RXCIE1);
    uint16_t res;
    if (rxEnd >= rxStart)
    {
        res = rxEnd - rxStart;
    }
    else res = rxEnd + RX_BUFFER_SIZE - rxStart;
    UCSR1B |= (1 << RXCIE1);
    return res;
}

static uint16_t txAvailable()
{
    UCSR1B &= ~(1 << UDRIE1);
    uint16_t res;
    if (txEnd >= txStart)
    {
        res = txEnd - txStart;
    }
    else res = txEnd + TX_BUFFER_SIZE - txStart;
    UCSR1B |= (1 << UDRIE1);
    return res;
}

static void rxPut(char ch)
{
    if (rxAvailable() >= RX_BUFFER_SIZE - 1)
        return;
    rxBuffer[rxEnd++] = ch;
    if (rxEnd >= RX_BUFFER_SIZE)
        rxEnd = 0;
}

static uint8_t rxGet(char *ch)
{
    if (ch == 0)
        return 0;
    if (rxAvailable() == 0)
        return 0;
    UCSR1B &= ~(1 << RXCIE1);
    *ch = rxBuffer[rxStart++];
    if (rxStart >= RX_BUFFER_SIZE)
        rxStart = 0;
    UCSR1B |= (1 << RXCIE1);
    return 1;
}

static uint8_t txGet(char *ch)
{
    if (ch == 0)
        return 0;
    if (txAvailable() == 0)
        return 0;
    UCSR1B &= ~(1 << UDRIE1);
    *ch = txBuffer[txStart++];
    if (txStart >= TX_BUFFER_SIZE)
        txStart = 0;
    UCSR1B |= (1 << UDRIE1);
    return 1;
}

static void txPutN(char *ch, uint16_t len)
{
    uint16_t maxLen = TX_BUFFER_SIZE - txAvailable() - 1;
    if (len == 0)
        return;
    if (len > maxLen)
        len = maxLen;
    UCSR1B &= ~(1 << UDRIE1);
    while(len--)
    {
        txBuffer[txEnd++] = *ch++;
        if (txEnd >= TX_BUFFER_SIZE)
            txEnd = 0;
    }
    UCSR1B |= (1 << UDRIE1);
}

static void txPut(char *ch)
{
    uint16_t len = strlen(ch);
	txPutN(ch, len);
}

ISR(USART1_RX_vect)
{
    rxPut(UDR1);
}

ISR(USART1_UDRE_vect)
{
    char ch;
    if (txGet(&ch))
        UDR1 = ch;
    else
        UCSR1B &= ~(1 << UDRIE1);
}

static void showHelp()
{
	txPut("\r\nHelp:\r\n");
	txPut(" i Set WiFi SSID\r\n");
	txPut(" p Set WiFi password\r\n");
	txPut(" s Set NTP server\r\n");
	txPut(" z Set timezone\r\n");
	txPut(" g Show all settings\r\n");
	txPut("> ");
}

static void showAllSettings()
{
	Settings settings;
	if (settings_read(&settings))
       settings_writeDefaults(&settings);
	txPut("SSID: ");
	txPut(settings.wifiSsid);
	txPut(", passwd: ");
	txPut(settings.wifiPass);
	txPut(", NTP: ");
	txPut(settings.ntpServer);
	txPut(", zone: ");
	char zone[16];
	intToStr(zone, settings.timeZone);
	txPut(zone);
	txPut("\r\n");
}

static void processCommand(RxState state)
{
	Settings settings;
	if (settings_read(&settings))
       settings_writeDefaults(&settings);
	switch(state)
	{
	case RXS_SSID:
		txPut("WiFi SSID set to ");
		txPutN(rxString, rxStrSize);
		txPut("\r\n");
		strcpy(settings.wifiSsid, rxString);
		break;

	case RXS_PASS:
		txPut("WiFi password set to ");
		txPutN(rxString, rxStrSize);
		txPut("\r\n");
		strcpy(settings.wifiPass, rxString);
		break;

	case RXS_NTP:
		txPut("NTP server set to ");
		txPutN(rxString, rxStrSize);
		txPut("\r\n");
		strcpy(settings.ntpServer, rxString);
		break;

	case RXS_ZONE:
		{
			int16_t zone = strToInt(rxString);
			char str[16];
			intToStr(str, zone);
			txPut("Timezone set to ");
			txPut(str);
			txPut("\r\n");
			settings.timeZone = zone;
		}
		break;

	default:
		break;
	}
	settings_write(&settings);
}

void uartusb_init()
{
    rxStart = 0;
    rxEnd = 0;
    txStart = 0;
    txEnd = 0;
    rxState = RXS_WAIT_CMD;
	rxPrevState = RXS_WAIT_CMD;
    // 19200 kbps
    UBRR1H = 0;
    UBRR1L = 25;
    UCSR1A = 0x00;
    UCSR1B = (1 << RXCIE1) | (1 << UDRIE1) | (1 << RXEN1) | (1 << TXEN1);
    UCSR1C = (1 << UCSZ11) | (1 << UCSZ10);
}

void uartusb_process()
{
	char ch;
	switch(rxState)
	{
	case RXS_WAIT_CMD:
		rxStrSize = 0;
		if (rxGet(&ch))
		{
			txPut("\r\n");
			if ((ch == '\r') || (ch == '\n'))
				showHelp();
			else if (ch == 'i')
				rxState = RXS_SSID;
			else if (ch == 'p')
				rxState = RXS_PASS;
			else if (ch == 's')
				rxState = RXS_NTP;
			else if (ch == 'z')
				rxState = RXS_ZONE;
			else if (ch == 'g')
				rxState = RXS_SHOW_ALL;
		}
		break;

	case RXS_SSID:
		rxPrevState = rxState;
		rxState = RXS_GET_STR;
		txPut("Enter SSID: ");
		break;

	case RXS_PASS:
		rxPrevState = rxState;
		rxState = RXS_GET_STR;
		txPut("Enter password: ");
		break;

	case RXS_NTP:
		rxPrevState = rxState;
		rxState = RXS_GET_STR;
		txPut("Enter NTP server address: ");
		break;

	case RXS_ZONE:
		rxPrevState = rxState;
		rxState = RXS_GET_STR;
		txPut("Enter timezone: ");
		break;

	case RXS_SHOW_ALL:
		showAllSettings();
		rxState = RXS_WAIT_CMD;
		txPut("> ");
		break;

	case RXS_GET_STR:
		if (rxGet(&ch))
		{
			if ((ch == '\r') || (ch == '\n'))
			{
				txPut("\r\n");
				processCommand(rxPrevState);
				rxState = RXS_WAIT_CMD;
				txPut("> ");
			}
			else if (rxStrSize < RX_MAX_STR_SIZE)
			{
				txPutN(&ch, 1);
				rxString[rxStrSize++] = ch;
			}
		}
		break;
	}
}

#else

void uartusb_init()
{
}

void uartusb_process()
{
}

#endif
