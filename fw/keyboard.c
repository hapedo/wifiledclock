#include "keyboard.h"
#include "platform.h"
#include "timer.h"

typedef struct
{
    uint8_t lastDown;
    Timestamp timeDown;
    Timestamp timeRepeat;
    uint32_t repeatPeriod;
} KeyDesc;

static KeyDesc keyDesc[__KEY_COUNT];

void keyboard_init()
{
    for(int i = 0; i < __KEY_COUNT; i++)
    {
        keyDesc[i].lastDown = 0;
        keyDesc[i].timeDown = 0;
        keyDesc[i].repeatPeriod = 0;
        keyDesc[i].timeRepeat = 0;
    }
    keyboard_setRepeatPeriod(KEY_MENU, 1000);
    keyboard_setRepeatPeriod(KEY_ESC, 1000);
    keyboard_setRepeatPeriod(KEY_UP, 250);
    keyboard_setRepeatPeriod(KEY_DOWN, 250);
}

void keyboard_process()
{
    for(int i = 0; i < __KEY_COUNT; i++)
    {
        uint8_t isDown;
        switch((Key)i)
        {
        case KEY_MENU:
            isDown = sw0_active();
            break;
        case KEY_ESC:
            isDown = sw1_active();
            break;
        case KEY_UP:
            isDown = sw3_active();
            break;
        case KEY_DOWN:
            isDown = sw2_active();
            break;
        default:
            isDown = 0;
            break;
        }
        if ((isDown) && (keyDesc[i].lastDown == 0))
        {
            keyDesc[i].timeDown = timer_timestamp();
            keyDesc[i].timeRepeat = keyDesc[i].timeDown;
        }
        keyDesc[i].lastDown = isDown;
    }
}

void keyboard_setRepeatPeriod(Key key, uint32_t milli)
{
    if (key >= __KEY_COUNT)
        return;
    keyDesc[key].repeatPeriod = milli;
}

uint8_t keyboard_isDown(Key key)
{
#if (USE_KEYBOARD == 1)
    if (key >= __KEY_COUNT)
        return 0;
    return keyDesc[key].lastDown;
#else
    return 0;
#endif
}

uint8_t keyboard_isDownFor(Key key, uint32_t milli)
{
#if (USE_KEYBOARD == 1)
    if (key >= __KEY_COUNT)
        return 0;
    if (keyDesc[key].lastDown == 0)
        return 0;
    if (timer_intervalMilli(keyDesc[key].timeDown, timer_timestamp()) >= milli)
        return 1;
#endif
    return 0;
}

uint8_t keyboard_isRepeatedEvent(Key key)
{
#if (USE_KEYBOARD == 1)
    if (key >= __KEY_COUNT)
        return 0;
    if (timer_hasPassed(keyDesc[key].timeRepeat) == 0)
        return 0;
    if (keyDesc[key].lastDown == 0)
        return 0;
    return 1;
#else
    return 0;
#endif
}

void keyboard_consumeKeyEvent(Key key)
{
    if (key >= __KEY_COUNT)
        return;
    keyDesc[key].timeRepeat = timer_getFutureMilli(keyDesc[key].repeatPeriod);
}

