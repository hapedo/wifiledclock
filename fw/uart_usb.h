#ifndef _UART_USB_H_
#define _UART_USB_H_

void uartusb_init();

void uartusb_process();

#endif // _UART_USB_H_
