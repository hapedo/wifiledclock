#ifndef _COMPAT_H_
#define _COMPAT_H_

// This header file provides compatibility definitions between 
// ATMEGA32 and ATMEGA644

#include <avr/io.h>

#ifndef TCCR0

#define TCCR0       TCCR0B
#define OCR0        OCR0A
#define TIMSK       TIMSK0

#define UCSRA       UCSR0A
#define UCSRB       UCSR0B
#define UCSRC       UCSR0C
#define UDR         UDR0
#define RXCIE       RXCIE0
#define UBRRH       UBRR0H
#define UBRRL       UBRR0L

#define RXC         RXC0
#define TXC         TXC0
#define UDRE        UDRE0
#define FE          FE0
#define DOR         DOR0
#define UPE         UPE0
#define U2X         U2X0
#define MPCM        MPCM0

#define RXCIE       RXCIE0
#define TXCIE       TXCIE0
#define UDRIE       UDRIE0
#define RXEN        RXEN0
#define TXEN        TXEN0
#define UCSZ        UCSZ02
#define RXB8        RXB80
#define TXB8        TXB80

#define UCSZ1       UCSZ01
#define UCSZ0       UCSZ00

#define USART_RXC_vect      USART0_RX_vect
#define USART_UDRE_vect     USART0_UDRE_vect
#define TIMER0_COMP_vect    TIMER0_COMPA_vect

#endif

#endif // _COMPAT_H_
