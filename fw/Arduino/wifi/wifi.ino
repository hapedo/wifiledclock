#include <stdlib.h>
#include <ESP8266WiFi.h>
#include <WiFiClient.h>
#include <NtpClientLib.h>
#include <MillisTimer.h>

#define LOG(...)        { Serial.printf(">L!"); Serial.printf(__VA_ARGS__); Serial.printf("\n"); }
#define replyCommand(C, ...)  { Serial.printf(">%c!", C); Serial.printf(__VA_ARGS__); Serial.printf("\n"); }

enum SerialState
{
  SS_IDLE = 0,
  SS_SYNC,
  SS_TYPE,
  SS_COMMAND,
  SS_CRLF
};

// vytvoření proměnných s názvem WiFi sítě a heslem
char paramSsid[32] = "HapNet";
char paramPass[32] = "todd5lon";
char ntpServer[32] = "0.pool.ntp.org";
int8_t timeZone = 1;
int8_t minutesTimeZone = 0;
bool dayLightEnable = true;

static int lastStatus = WL_IDLE_STATUS;
boolean syncEventTriggered = false; // True if a time even has been triggered
NTPSyncEvent_t ntpEvent; // Last triggered event
MillisTimer timer1 = MillisTimer(2000);

void processSyncEvent (NTPSyncEvent_t ntpEvent) 
{
    if (ntpEvent) 
    {
        if (ntpEvent == noResponse)
        {
            LOG("Time Sync error: NTP server not reachable");
        }
        else if (ntpEvent == invalidAddress)
        {
            LOG("Time Sync error: Invalid NTP server address");
        }
    } 
    else 
    {
        LOG("Got NTP time");
        time_t tm = NTP.getLastNTPSync();
        //replyCommand('T', NTP.getTimeDateString (NTP.getLastNTPSync ()).c_str());
        uint8_t daylight = 0;
        if (NTP.getDayLight())
            daylight = 1;
        LOG("NTP: %d", tm);
        if ((year(tm) >= 2018) && (year(tm) <= 2030))
            replyCommand('T', "%02u%02u%02u%02u%02u%02u%01u", hour(tm), minute(tm), second(tm), day(tm), month(tm), year(tm) % 100, daylight);
    }
}

void onSTAGotIP(WiFiEventStationModeGotIP ipInfo) 
{
  LOG("Got IP: %s\r", ipInfo.ip.toString().c_str());
}

void onSTADisconnected(WiFiEventStationModeDisconnected event_info) 
{
  LOG("Disconnected");
}

void timerCallback(MillisTimer &mt)
{
  if (WiFi.status() == WL_CONNECTED)
  {
    LOG("NTP request");
    NTP.begin (ntpServer, timeZone, dayLightEnable, minutesTimeZone);
  }
}

void setup() 
{
  static WiFiEventHandler e1, e2;

  paramSsid[0] = 0;
  paramPass[0] = 0;

  Serial.begin(19200);

  delay(10);
  
  LOG("Power up");

  WiFi.setAutoConnect(false);
  //WiFi.begin(paramSsid, paramPass);

  NTP.onNTPSyncEvent ([](NTPSyncEvent_t event) {
        ntpEvent = event;
        syncEventTriggered = true;
  });

  e1 = WiFi.onStationModeGotIP(onSTAGotIP);// As soon WiFi is connected, start NTP Client
  e2 = WiFi.onStationModeDisconnected(onSTADisconnected);

  timer1.setInterval(2000);
  timer1.expiredHandler(timerCallback);
  timer1.setRepeats(0);
  timer1.start();
}

void processCommand(uint8_t command, bool isQuerry, uint8_t* param, uint8_t paramSize)
{
  if (command == 'S')
  {
    if (isQuerry)
    {
      replyCommand('S', paramSsid);
    }
    else
    {
      memcpy(paramSsid, param, paramSize);
      paramSsid[paramSize] = 0;
      LOG("SSID set to %s", paramSsid);
    }
  }
  else if (command == 'P')
  {
    if (isQuerry)
    {
      replyCommand('P', paramPass);
    }
    else
    {
      memcpy(paramPass, param, paramSize);
      paramPass[paramSize] = 0;
      LOG("Pass set to %s", paramPass);
    }
  }
  else if (command == 'C')
  {
    WiFi.mode(WIFI_AP_STA);
    WiFi.begin(paramSsid, paramPass);
    WiFi.setAutoReconnect(true);
    LOG("Connect");
  }
  else if (command == 'D')
  {
    WiFi.setAutoReconnect(false);
    WiFi.disconnect();
    LOG("Disconnect");
  }
  else if (command == 'Z')
  {
    timeZone = atoi((char*)param);
    LOG("Timezone set to %d", timeZone);
  }
  else if (command == 'N')
  {
      memcpy(ntpServer, param, paramSize);
      ntpServer[paramSize] = 0;
      LOG("Server set to %s", ntpServer);
  }
  else if (command == 'M')
  {
      if (param[0] == '0')
      {
         LOG("DLT disabled", timeZone);
         dayLightEnable = false;
      }
      else
      {
         LOG("DLT enabled", timeZone);
         dayLightEnable = true;
      }
  }
}

void processSerial()
{
  static SerialState state = SS_IDLE;
  static bool isQuerry;
  static uint8_t cmd;
  static uint8_t param[64];
  static uint8_t paramSize = 0;

  while (Serial.available())
  {
    uint8_t val = Serial.read();
    switch(state)
    {
      case SS_IDLE:
        if (val == '>')
          state = SS_SYNC;
        break;
      case SS_SYNC:
        cmd = val;
        state = SS_TYPE;
        paramSize = 0;
        break;
      case SS_TYPE:
        if (val == '?')
          isQuerry = true;
        else if (val == '!')
          isQuerry = false;
        else
        {
          state = SS_SYNC;
          break;
        }
        state = SS_COMMAND;
        break;
      case SS_COMMAND:
        if ((val == '\r') || (val == '\n') || (paramSize >= sizeof(param) - 1))
        {
          param[paramSize] = 0;
          processCommand(cmd, isQuerry, param, paramSize);
          state = SS_IDLE;
          break;
        }
        param[paramSize++] = val;
        break;
    }
  }
}

void loop() {
  // put your main code here, to run repeatedly:

  processSerial();
  timer1.run();

  if (WiFi.status() != lastStatus)
  {
    lastStatus = WiFi.status();
    switch(lastStatus)
    {
      case WL_CONNECTED:
        LOG("WiFi connected");
        NTP.begin (ntpServer, timeZone, dayLightEnable, minutesTimeZone);
        NTP.setInterval(10, 10);
        break;

      default:
        LOG("WiFi not connected");
        break;  
    }
  }

  if (syncEventTriggered) 
  {
        processSyncEvent (ntpEvent);
        syncEventTriggered = false;
  }
}
