#include "display.h"
#include <stdint.h>
#include "platform.h"

#define BRIGHTNESS_CHG_DIV      500

static const uint8_t convert[11] = { 0x3f, 0x06, 0x5b, 0x4f, 0x66, 0x6d, 0x7d, 0x07, 0x7f, 0x6f, 0 };
static uint8_t segmentNo;
static uint8_t counter;
static uint8_t segment[4];
static uint8_t brightness;
static uint8_t targetBrightness;
static uint8_t dotVisible;
static uint8_t zeroHidden;

static void setOutputs(uint8_t value)
{
    value & 0x01 ? ca_on() : ca_off();
    value & 0x02 ? cb_on() : cb_off();
    value & 0x04 ? cc_on() : cc_off();
    value & 0x08 ? cd_on() : cd_off();
    value & 0x10 ? ce_on() : ce_off();
    value & 0x20 ? cf_on() : cf_off();
    value & 0x40 ? cg_on() : cg_off();
}

void display_init()
{
    segmentNo = 0;
    counter = 0;
    dotVisible = 0;
    brightness = 1;
    targetBrightness = 1;
    zeroHidden = 1;
    segment[0] = 10;
    segment[1] = 10;
    segment[2] = 10;
    segment[3] = 10;
}

void display_process()
{
    static uint16_t brightnessDiv = 0;

    brightnessDiv++;
    if (brightnessDiv >= BRIGHTNESS_CHG_DIV)
    {
        brightnessDiv = 0;
        if (targetBrightness > brightness)
            brightness++;
        if (targetBrightness < brightness)
            brightness--;
    }

    counter++;
    if (counter >= DISPLAY_BRIGHTNESS_MAX)
    {
        segmentNo++;
        if (segmentNo >= SEGMENT_COUNT)
            segmentNo = 0;
        counter = 0;
    }

    ca_off();
    cb_off();
    cc_off();
    cd_off();
    ce_off();
    cf_off();
    cg_off();
    cdot_off();

    segmentNo == 0 ? a0_on() : a0_off();
    segmentNo == 1 ? a1_on() : a1_off();
    segmentNo == 2 ? a2_on() : a2_off();
    segmentNo == 3 ? a3_on() : a3_off();

    if (counter <= brightness)
    {
        if ((segmentNo > 0) || (segment[0] != 0) || (zeroHidden == 0))
            setOutputs(convert[segment[segmentNo]]);
        if ((dotVisible) && (segmentNo == 0))
            cdot_on();
    }
}

void display_setSegment(uint8_t segmentNo, uint8_t value)
{
    if (segmentNo >= SEGMENT_COUNT)
        return;
    segment[segmentNo] = value;
}

void display_setBrightness(uint8_t value)
{
    targetBrightness = value;
}

void display_setDotVisible(uint8_t visible)
{
    dotVisible = visible;
}

uint8_t display_isDotVisible()
{
    return dotVisible;
}

void display_hideFirstZero(uint8_t hide)
{
    zeroHidden = hide;
}
