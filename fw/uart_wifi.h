#ifndef _UART_WIFI_H_
#define _UART_WIFI_H_

void uartwifi_init();

void uartwifi_process();

#endif // _UART_WIFI_H_
