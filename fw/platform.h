#ifndef _PLATFORM_H_
#define _PLATFORM_H_

#include <avr/io.h>


// PortA definitions
#define A2                      0x01
#define A1                      0x02
#define A0                      0x04
#define SW0                     0x08
#define SW1                     0x10
#define SW2                     0x20
#define SW3                     0x40
#define LIGHT                   0x80


#define PORTA_OUTPUT			(A2 | A1 | A0 )
#define PORTA_DEFAULT_H			(0)

// PortB definitions
#define A3                      0x01
#define CG                      0x02
#define CF                      0x04
#define CE                      0x08 
#define CD                      0x10
#define CC                      0x20
#define CB                      0x40
#define CA                      0x80

#define PORTB_OUTPUT			(CA | CB | CC | CD | CE | CF | CG | A3)
#define PORTB_DEFAULT_H			(0)

// PortC definitions
#define CDOT                    0x80
#define LED_ERR                 0x40
#define SDA                     0x02
#define SCL                     0x01


#define PORTC_OUTPUT			(CDOT | LED_ERR | SCL)
#define PORTC_DEFAULT_H			(0)

// PortD definitions
#define LED_DBG                 0x80
#define PW_WIFI                 0x40
#define RST_WIFI                0x20
#define EN_WIFI                 0x10
#define TX_USB                  0x08
#define RX_USB                  0x04
#define TX_WIFI                 0x02
#define RX_WIFI                 0x01

#define PORTD_OUTPUT			(LED_DBG | PW_WIFI | RST_WIFI | EN_WIFI | TX_USB | TX_WIFI)
#define PORTD_DEFAULT_H			(0)

// PortA macros
#define a0_on()                (PORTA |= A0)
#define a0_off()               (PORTA &= ~A0)
#define a1_on()                (PORTA |= A1)
#define a1_off()               (PORTA &= ~A1)
#define a2_on()                (PORTA |= A2)
#define a2_off()               (PORTA &= ~A2)
#define sw0_active()           ((PINA & SW0) == 0)
#define sw1_active()           ((PINA & SW1) == 0)
#define sw2_active()           ((PINA & SW2) == 0)
#define sw3_active()           ((PINA & SW3) == 0)

// PortB macros
#define a3_on()                (PORTB |= A3)
#define a3_off()               (PORTB &= ~A3)
#define ca_on()                (PORTB |= CA)
#define ca_off()               (PORTB &= ~CA)
#define cb_on()                (PORTB |= CB)
#define cb_off()               (PORTB &= ~CB)
#define cc_on()                (PORTB |= CC)
#define cc_off()               (PORTB &= ~CC)
#define cd_on()                (PORTB |= CD)
#define cd_off()               (PORTB &= ~CD)
#define ce_on()                (PORTB |= CE)
#define ce_off()               (PORTB &= ~CE)
#define cf_on()                (PORTB |= CF)
#define cf_off()               (PORTB &= ~CF)
#define cg_on()                (PORTB |= CG)
#define cg_off()               (PORTB &= ~CG)

// PortC macros
#define cdot_on()              (PORTC |= CDOT)
#define cdot_off()             (PORTC &= ~CDOT) 
#define lederr_on()            (PORTC |= LED_ERR)
#define lederr_off()           (PORTC &= ~LED_ERR)
#define lederr_toggle()        (PORTC ^= LED_ERR)

// PortD macros
#define ledDbg_on()             (PORTD |= LED_DBG)
#define ledDbg_off()            (PORTD &= ~LED_DBG)
#define ledDbg_toggle()         (PORTD ^= LED_DBG)
#define wifi_on()               (PORTD |= PW_WIFI)
#define wifi_off()              (PORTD &= ~PW_WIFI)
#define wifi_rstActive()        (PORTD &= ~RST_WIFI)
#define wifi_rstInactive()      (PORTD |= RST_WIFI)
#define wifi_enable()           (PORTD |= EN_WIFI)
#define wifi_disable()          (PORTD &= ~EN_WIFI)

void platform_init();

#endif // _PLATFORM_H_
