#include "uart_wifi.h"
#include "config.h"

#if (USE_WIFI == 1)

#include <stdint.h>
#include <string.h>
#include <avr/io.h>
#include <avr/interrupt.h>
#include "compat.h"
#include "platform.h"
#include "settings.h"
#include "timer.h"
#include "utils.h"
#include "ds3231.h"

#define RX_BUFFER_SIZE          32
#define TX_BUFFER_SIZE          32
#define RX_COMMAND_DATA_SIZE    32

#define WAIT_NTP_TIME_TIMEOUT   (5 * 60000)
#define WAIT_NTP_PERIOD         (10 * 60000)

typedef enum
{
    RXS_WAIT_SYNC,
    RXS_COMMAND,
    RXS_TYPE,
    RXS_CRLF
} RxState;

typedef enum
{
    NS_IDLE,
    NS_POWERUP,
    NS_SSID,
    NS_PASS,
    NS_SERVER,
    NS_TIMEZONE,
    NS_CONNECT,
    NS_WAITTIME,
    NS_WAIT
} NtpState;

static char rxBuffer[RX_BUFFER_SIZE];
static char txBuffer[TX_BUFFER_SIZE];
static uint16_t rxStart;
static uint16_t rxEnd;
static uint16_t txStart;
static uint16_t txEnd;
static RxState rxState;
static uint8_t rxCommand;
static uint8_t rxIsQuery;
static uint8_t rxData[RX_COMMAND_DATA_SIZE];
static uint16_t rxDataSize;
static Timestamp ntpTimer = 0;
static Settings settings;
static NtpState ntpState = NS_IDLE;

static uint16_t rxAvailable()
{
    UCSRB &= ~(1 << RXCIE);
    uint16_t res;
    if (rxEnd >= rxStart)
    {
        res = rxEnd - rxStart;
    }
    else res = rxEnd + RX_BUFFER_SIZE - rxStart;
    UCSRB |= (1 << RXCIE);
    return res;
}

static uint16_t txAvailable()
{
    UCSRB &= ~(1 << UDRIE);
    uint16_t res;
    if (txEnd >= txStart)
    {
        res = txEnd - txStart;
    }
    else res = txEnd + TX_BUFFER_SIZE - txStart;
    UCSRB |= (1 << UDRIE);
    return res;
}

static void rxPut(char ch)
{
    if (rxAvailable() >= RX_BUFFER_SIZE - 1)
        return;
    rxBuffer[rxEnd++] = ch;
    if (rxEnd >= RX_BUFFER_SIZE)
        rxEnd = 0;
}

static uint8_t rxGet(char *ch)
{
    if (ch == 0)
        return 0;
    if (rxAvailable() == 0)
        return 0;
    UCSRB &= ~(1 << RXCIE);
    *ch = rxBuffer[rxStart++];
    if (rxStart >= RX_BUFFER_SIZE)
        rxStart = 0;
    UCSRB |= (1 << RXCIE);
    return 1;
}

static uint8_t txGet(char *ch)
{
    if (ch == 0)
        return 0;
    if (txAvailable() == 0)
        return 0;
    UCSRB &= ~(1 << UDRIE);
    *ch = txBuffer[txStart++];
    if (txStart >= TX_BUFFER_SIZE)
        txStart = 0;
    UCSRB |= (1 << UDRIE);
    return 1;
}

static void txPut(char *ch)
{
    uint16_t maxLen = TX_BUFFER_SIZE - txAvailable() - 1;
    uint16_t len = strlen(ch);
    if (len == 0)
        return;
    if (len > maxLen)
        len = maxLen;
    UCSRB &= ~(1 << UDRIE);
    while(len--)
    {
        txBuffer[txEnd++] = *ch++;
        if (txEnd >= TX_BUFFER_SIZE)
            txEnd = 0;
    }
    UCSRB |= (1 << UDRIE);
}

static void processCommand()
{
    if (rxCommand == 'T')
    {
        char str[5];
        Time time;
        str[0] = rxData[0];
        str[1] = rxData[1];
        str[2] = 0;
        time.hours = strToUInt(str);
        str[0] = rxData[2];
        str[1] = rxData[3];
        time.minutes = strToUInt(str);
        str[0] = rxData[4];
        str[1] = rxData[5];
        time.seconds = strToUInt(str);
        ds3231_setTime(&time);
        ntpTimer = timer_getFutureMilli(WAIT_NTP_PERIOD);
        ntpState = NS_IDLE;
        wifi_off();
        lederr_off();
        asm("nop");
    }
}

ISR(USART_RXC_vect)
{
    rxPut(UDR);
}

ISR(USART_UDRE_vect)
{
    char ch;
    if (txGet(&ch))
        UDR = ch;
    else
        UCSRB &= ~(1 << UDRIE);
}

void uartwifi_init()
{
    rxStart = 0;
    rxEnd = 0;
    txStart = 0;
    txEnd = 0;
    rxState = RXS_WAIT_SYNC;
    rxDataSize = 0;
    // 19200 kbps
    UBRRH = 0;
    UBRRL = 25;
    UCSRA = 0x00;
    UCSRB = (1 << RXCIE) | (1 << UDRIE) | (1 << RXEN) | (1 << TXEN);
#ifdef __AVR_ATmega32__
    UCSRC = (1 << URSEL) | (1 << UCSZ1) | (1 << UCSZ0);
#else
    UCSRC = (1 << UCSZ1) | (1 << UCSZ0);
#endif
}

void uartwifi_process()
{
    static NtpState nextState = NS_IDLE;
    static Timestamp timer;

    char ch;
    while (rxGet(&ch))
    {
        if (ch == '>')
        {
            rxState = RXS_COMMAND;
            continue;
        }
        switch(rxState)
        {
        case RXS_COMMAND:
            rxCommand = ch;
            rxState = RXS_TYPE;
            break;

        case RXS_TYPE:
            if (ch == '?')
                rxIsQuery = 1;
            else if (ch == '!')
                rxIsQuery = 0;
            else
            {
                rxState = RXS_WAIT_SYNC;
                continue;
            }
            rxState = RXS_CRLF;
            rxDataSize = 0;
            break;

        case RXS_CRLF:
            if ((ch == '\r') || (ch == '\n'))
            {
                processCommand();
                rxState = RXS_WAIT_SYNC;
            }
            else
            {
                if (rxDataSize <= RX_COMMAND_DATA_SIZE)
                    rxData[rxDataSize++] = ch;
            }
            break;

        default:
            break;
        }
    }

    switch(ntpState)
    {
    case NS_IDLE:
        if (timer_hasPassed(ntpTimer))
            ntpState = NS_POWERUP;
        break;

    case NS_POWERUP:
        if (settings_read(&settings))
            settings_writeDefaults(&settings);
        wifi_enable();
        wifi_rstInactive();
        wifi_on();
        timer = timer_getFutureMilli(5000);
        nextState = NS_SSID;
        ntpState = NS_WAIT;
        break;

    case NS_SSID:
        txPut(">S!");
        txPut(settings.wifiSsid);
        txPut("\n");
        timer = timer_getFutureMilli(0);
        nextState = NS_PASS;
        ntpState = NS_WAIT;
        break;

    case NS_PASS:
        txPut(">P!");
        txPut(settings.wifiPass);
        txPut("\n");
        timer = timer_getFutureMilli(0);
        nextState = NS_SERVER;
        ntpState = NS_WAIT;
        break;

    case NS_SERVER:
        txPut(">N!");
        txPut(settings.ntpServer);
        txPut("\n");
        timer = timer_getFutureMilli(0);
        nextState = NS_TIMEZONE;
        ntpState = NS_WAIT;
        break;

    case NS_TIMEZONE:
		{
			char str[16];
			intToStr(str, settings.timeZone);
        	txPut(">Z!");
        	txPut(str);
        	txPut("\n");
        	timer = timer_getFutureMilli(0);
        	nextState = NS_CONNECT;
        	ntpState = NS_WAIT;
		}
        break;

    case NS_CONNECT:
        txPut(">C!\n");
        timer = timer_getFutureMilli(0);
        ntpTimer = timer_getFutureMilli(WAIT_NTP_TIME_TIMEOUT);
        nextState = NS_WAITTIME;
        ntpState = NS_WAIT;
        break;

    case NS_WAITTIME:
        if (timer_hasPassed(ntpTimer))
        {
            ntpTimer = timer_getFutureMilli(WAIT_NTP_PERIOD);
            ntpState = NS_IDLE;
            wifi_off();
            lederr_on();
        }
        break;

    case NS_WAIT:
        if (txAvailable())
            break;
        if (timer_hasPassed(timer))
            ntpState = nextState;
        break;
    }
}

#else // USE_WIFI

void uartwifi_init()
{
}

void uartwifi_process()
{
}

#endif
