#include "ds3231.h"
#include "i2c_master.h"

static uint8_t toBCD(uint8_t val)
{
    uint8_t res = val % 10;
    res |= (val / 10) << 4;
    return res;
}

void ds3231_init()
{
    // Default CR - start oscillator
    uint8_t cr = 0x1c;
    i2c_writeReg(0xd0, 0, &cr, 1);
}

void ds3231_getTimeBCD(Time *time)
{
    uint8_t buffer[3];
    i2c_readReg(0xd0, 0, buffer, 3);
    time->seconds = buffer[0];
    time->minutes = buffer[1];
    time->hours = buffer[2] & 0x3f;
}

void ds3231_setTime(Time *time)
{
    uint8_t buffer[3];
    buffer[0] = toBCD(time->seconds);
    buffer[1] = toBCD(time->minutes);
    buffer[2] = toBCD(time->hours) & 0x3f;
    i2c_writeReg(0xd0, 0, buffer, 3);
}

void ds3231_setTimeBCD(Time *time)
{
    uint8_t buffer[3];
    buffer[0] = time->seconds;
    buffer[1] = time->minutes;
    buffer[2] = (time->hours) & 0x3f;
    i2c_writeReg(0xd0, 0, buffer, 3);
}
