#ifndef _KEYBOARD_H_
#define _KEYBOARD_H_

#include <stdint.h>
#include "config.h"

#ifndef USE_KEYBOARD
#define USE_KEYBOARD            0
#endif

typedef enum
{
    KEY_MENU = 0,
    KEY_ESC,
    KEY_UP,
    KEY_DOWN,
    __KEY_COUNT,
} Key;

void keyboard_init();

void keyboard_process();

void keyboard_setRepeatPeriod(Key key, uint32_t milli);

uint8_t keyboard_isDown(Key key);

uint8_t keyboard_isDownFor(Key key, uint32_t milli);

uint8_t keyboard_isRepeatedEvent(Key key);

void keyboard_consumeKeyEvent(Key key);

#endif // _KEYBOARD_H_
