#include <avr/interrupt.h>
#include "platform.h"
#include "timer.h"
#include "i2c_master.h"
#include "display.h"
#include "ds3231.h"
#include "uart_wifi.h"
#include "uart_usb.h"
#include "keyboard.h"
#include "adc.h"
#include "settings.h"

typedef enum
{
    MENU_NONE = 0,
    MENU_MINUTES,
    MENU_HOURS
} MenuState;

static Settings settings;
static MenuState menuState;
static MenuState lastMenuState;
static uint8_t valueVisible;

uint8_t isValueKeyEvent(Key key)
{
    uint8_t event = 0;
    if (keyboard_isDownFor(key, 4000))
    {
        if (keyboard_isRepeatedEvent(key))
        {
            keyboard_consumeKeyEvent(key);
            keyboard_setRepeatPeriod(key, 100);
            event = 1;  
        }
    }
    else if (keyboard_isDownFor(key, 500))
    {
        if (keyboard_isRepeatedEvent(key))
        {
            keyboard_consumeKeyEvent(key);
            keyboard_setRepeatPeriod(key, 250);
            event = 1;
        }
    }
    else if (keyboard_isDownFor(key, 100))
    {
        if (keyboard_isRepeatedEvent(key))
        {
            keyboard_consumeKeyEvent(key);
            keyboard_setRepeatPeriod(key, 500);
            event = 1;
        }
    }
    else
    {
        keyboard_setRepeatPeriod(key, 500);
    }
    return event;
}

int main()
{
    platform_init();
    timer_init();
    i2c_init();
    display_init();
    ds3231_init();
    uartwifi_init();
	uartusb_init();
    keyboard_init();
	adc_init();

    menuState = MENU_NONE;
    lastMenuState = MENU_NONE;
    valueVisible = 1;

    if (settings_read(&settings))
        settings_writeDefaults(&settings);

    sei();

/*    Time time;
    time.seconds = 0;
    time.minutes = 22;
    time.hours = 22;
    ds3231_setTime(&time);*/

    Timestamp timer = 0;
    Time time;
    ds3231_getTimeBCD(&time);

    display_setBrightness(DISPLAY_BRIGHTNESS_MAX);

    while(1)
    {
		adc_process();
        uartwifi_process();
		uartusb_process();
        keyboard_process();
        if (menuState == MENU_NONE)
        {
            if (timer_hasPassed(timer))
            {
                timer = timer_getFutureMilli(500);
                ledDbg_toggle();

                if (display_isDotVisible())
                    display_setDotVisible(0);
                else
                {
                    display_setDotVisible(1);

                    ds3231_getTimeBCD(&time);
                    display_setSegment(0, time.hours >> 4);
                    display_setSegment(1, time.hours & 0x0f);
                    display_setSegment(2, time.minutes >> 4);
                    display_setSegment(3, time.minutes & 0x0f);
                }
            }
        }
        else
        {
            display_setDotVisible(1);
            if (timer_hasPassed(timer))
            {
                timer = timer_getFutureMilli(250);
                if (menuState == MENU_MINUTES)
                {
                    if (valueVisible)
                    {
                        display_setSegment(2, 10);
                        display_setSegment(3, 10);
                        valueVisible = 0;
                    }
                    else
                    {
                        display_setSegment(2, time.minutes >> 4);
                        display_setSegment(3, time.minutes & 0x0f);
                        valueVisible = 1;
                    }
                }
                else
                {
                    if (valueVisible)
                    {
                        display_setSegment(0, 10);
                        display_setSegment(1, 10);
                        valueVisible = 0;
                    }
                    else
                    {
                        display_setSegment(0, time.hours >> 4);
                        display_setSegment(1, time.hours & 0x0f);
                        valueVisible = 1;
                    }
                }
            }
            if (menuState == MENU_MINUTES)
            {
                display_setSegment(0, time.hours >> 4);
                display_setSegment(1, time.hours & 0x0f);
            }
            else
            {
                display_setSegment(2, time.minutes >> 4);
                display_setSegment(3, time.minutes & 0x0f);
            }
        }

        switch(menuState)
        {
        case MENU_NONE:
            if (keyboard_isDownFor(KEY_MENU, 100) && keyboard_isRepeatedEvent(KEY_MENU))
            {
                keyboard_consumeKeyEvent(KEY_MENU);
                menuState = MENU_MINUTES;
                timer = timer_getFutureMilli(250);
                valueVisible = 1;
            }
            break;

        case MENU_MINUTES:
            if (keyboard_isDownFor(KEY_ESC, 100) && keyboard_isRepeatedEvent(KEY_ESC))
            {
                keyboard_consumeKeyEvent(KEY_ESC);
                menuState = MENU_NONE;
                ds3231_setTimeBCD(&time);
            }
            if (keyboard_isDownFor(KEY_MENU, 100) && keyboard_isRepeatedEvent(KEY_MENU))
            {
                keyboard_consumeKeyEvent(KEY_MENU);
                menuState = MENU_HOURS;
                timer = timer_getFutureMilli(250);
                valueVisible = 1;
            }
            if (isValueKeyEvent(KEY_UP))
            {
                time.minutes++;
                if ((time.minutes & 0x0f) >= 10)
                {
                    time.minutes = (time.minutes & 0xf0) + 0x10;
                }
                if ((time.minutes & 0xf0) >= 0x60)
                    time.minutes = 0;
            }
            else if (isValueKeyEvent(KEY_DOWN))
            {
                time.minutes--;
                if ((time.minutes & 0x0f) == 0x0f)
                    time.minutes = (time.minutes & 0xf0) | 0x09;
                if ((time.minutes & 0xf0) == 0xf0)
                    time.minutes = 0x59;
            }
            break;



        case MENU_HOURS:
            if (keyboard_isDownFor(KEY_ESC, 100) && keyboard_isRepeatedEvent(KEY_ESC))
            {
                keyboard_consumeKeyEvent(KEY_ESC);
                menuState = MENU_NONE;
                ds3231_setTimeBCD(&time);
            }
            if (keyboard_isDownFor(KEY_MENU, 100) && keyboard_isRepeatedEvent(KEY_MENU))
            {
                keyboard_consumeKeyEvent(KEY_MENU);
                menuState = MENU_MINUTES;
                timer = timer_getFutureMilli(250);
                valueVisible = 1;
            }
            if (isValueKeyEvent(KEY_UP))
            {
                time.hours++;
                if ((time.hours & 0x0f) >= 10)
                {
                    time.hours = (time.hours & 0xf0) + 0x10;
                }
                if (time.hours >= 0x24)
                    time.hours = 0;
            }
            else if (isValueKeyEvent(KEY_DOWN))
            {
                time.hours--;
                if ((time.hours & 0x0f) == 0x0f)
                    time.hours = (time.hours & 0xf0) | 0x09;
                if ((time.hours & 0xf0) == 0xf0)
                    time.hours = 0x23;
            }
            break;
        }

        if (((lastMenuState != MENU_NONE) && (menuState == MENU_NONE)) ||
            ((menuState != MENU_NONE) && (keyboard_isDown(KEY_UP) || keyboard_isDown(KEY_DOWN))))
        {
            display_setSegment(0, time.hours >> 4);
            display_setSegment(1, time.hours & 0x0f);
            display_setSegment(2, time.minutes >> 4);
            display_setSegment(3, time.minutes & 0x0f);
        }
        lastMenuState = menuState;
    }
}
