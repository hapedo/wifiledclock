#ifndef _UTILS_H_
#define _UTILS_H_

#include <stdint.h>

uint16_t calculateChecksum(uint8_t *data, uint16_t size);

uint16_t strToUInt(char *str);

int16_t strToInt(char *str);

void intToStr(char *dest, int16_t val);

#endif // _UTILS_H_
