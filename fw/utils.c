#include "utils.h"


uint16_t calculateChecksum(uint8_t *data, uint16_t size)
{
    uint16_t checksum = 0;
    uint16_t temp = 0;
    while(size--)
    {
        temp |= *data++;
        checksum += temp ^ size;
        temp <<= 8;
    }
    return checksum;
}

uint16_t strToUInt(char *str)
{
    uint16_t v = 0;
    if (str == 0)
        return v;
    while(*str != 0)
    {
        v = v * 10 + *str - '0';
        str++;
    }
    return v;
}

int16_t strToInt(char *str)
{
    int16_t v = 0;
	int16_t sign = 1;
	uint8_t wasSign = 0;
    if (str == 0)
        return v;
    while(*str != 0)
    {
		if (*str == '-')
			sign = -1;
		else if (*str == '+')
			sign = 1;
		else if ((*str >= '0') && (*str <= '9'))
	        v = v * 10 + *str - '0';
		else
			break;
        str++;
		wasSign = 1;
    }
    return v * sign;
}

void intToStr(char *dest, int16_t val)
{
	int16_t div = 10000;
	uint8_t conv = 0;
	if (val < 0)
	{
		*dest++ = '-';
		val *= -1;
	}
	while(val)
	{
		int16_t r = val / div;
		val %= div;
		div /= 10;
		if (r)
		{
			*dest++ = r + '0';
			conv = 1;
		}
	}
	if (conv == 0)
		*dest++ = '0';
	*dest = 0;
}

