#include "timer.h"
#include <stdint.h>
#include <avr/io.h>
#include <avr/interrupt.h>
#include "compat.h"
#include "adc.h"
#include "display.h"

#define msToTicks(ms)			(ms)
#define ticksToMs(ticks)		(ticks)

static Timestamp currTimestamp;

void timer_init()
{
	currTimestamp = 0;

	TCNT0 = 0;
	OCR0 = 25;
	TCCR0 = 0x03;
	TIMSK |= 0x02;
}

Timestamp timer_timestamp()
{
    return currTimestamp;
}

uint8_t timer_hasPassed(Timestamp timestamp)
{
    if (timestamp <= currTimestamp)
        return 1;
    else
        return 0;
}

Timestamp timer_getFutureMilli(uint32_t milli)
{
    return currTimestamp + msToTicks(milli); 
}

uint32_t timer_intervalMilli(Timestamp ts1, Timestamp ts2)
{
    if (ts2 < ts1)
        return 0;
    return ticksToMs(ts2 - ts1);
}

SIGNAL(TIMER0_COMP_vect)
{
    static uint8_t divider = 0;

	TCNT0 = 0;
    divider++;
    if (divider >= 5)
    {
	    currTimestamp++;
        divider = 0;
    }

    //adc_process();
    display_process();
}
