#ifndef _SETTINGS_H_
#define _SETTINGS_H_

#include <stdint.h>

#define SETTINGS_WIFISSID_LENGTH        16
#define SETTINGS_WIFIPASS_LENGTH        16
#define SETTINGS_NTPSERV_LENGTH         16

typedef __attribute__ ((packed)) struct
{
    char wifiSsid[SETTINGS_WIFISSID_LENGTH + 1];
    char wifiPass[SETTINGS_WIFIPASS_LENGTH + 1];
    char ntpServer[SETTINGS_NTPSERV_LENGTH + 1];
    int8_t timeZone;
    uint8_t daylightSavingTime;
    uint16_t checksum;
} Settings;

/*! Read settings from on-chip eeprom
 *  \param settings Destination
 *  \return 0 on success, -1 otherwise (invalid checksum)
 */
int settings_read(Settings *settings);

/*! Write default settings to on-chip eeprom
 *  \param settings Destination for defaults (can be 0)
 */
void settings_writeDefaults(Settings *settings);

/*! Write settings to on-chip eeprom
 *  \param settings Source
 */
void settings_write(Settings *settings);

#endif // _SETTINGS_H_
