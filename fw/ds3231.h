#ifndef _DS3231_H_
#define _DS3231_H_

#include <stdint.h>

//! Time structure
typedef struct
{
    uint8_t seconds;
    uint8_t minutes;
    uint8_t hours;
} Time;

//! Initialize DS3231 (enable clock)
void ds3231_init();

/*! Get time in BCD format
 *  \param time Destination. Members will be filled with BCD format
 */
void ds3231_getTimeBCD(Time *time);

/*! Set time (decimal format)
 *  \param time Time source (members with decimal format)
 */
void ds3231_setTime(Time *time);

/*! Set time in BCD format
 *  \param time Time source (members with BCD format)
 */
void ds3231_setTimeBCD(Time *time);

#endif // _DS3231_H_
