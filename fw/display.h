#ifndef _DISPLAY_H_
#define _DISPLAY_H_

#include <stdint.h>

//! Segment count
#define SEGMENT_COUNT               4
//! Maximal display brightness value
#define DISPLAY_BRIGHTNESS_MAX      15

//! Initialize display module
void display_init();

//! Process display module (shall be called from IRQ with fixed frequency)
void display_process();

/*! Set segment content
 *  \param segmentNo Segment number from 0 to SEGMENT_COUNT - 1
 *  \param value Value to show
 */
void display_setSegment(uint8_t segmentNo, uint8_t value);

/*! Set display brightness
 *  \param value Display brightness from 0 (off) to DISPLAY_BRIGHTNESS_MAX
 */
void display_setBrightness(uint8_t value);

/*! Set second dot visible or not
 *  \param visible 1 = visible, 0 = off
 */
void display_setDotVisible(uint8_t visible);

/*! Check whether second dot is visible
 *  \return 0 when off, 1 when visible
 */
uint8_t display_isDotVisible();

/*! Force hide segment 0 when zero
 *  \param hide 1 - segment 0 is off when zero
 */
void display_hideFirstZero(uint8_t hide);

#endif // _DISPLAY_H_
